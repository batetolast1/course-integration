package pl.softwareskill.course.caching.distributed;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import pl.softwareskill.course.caching.distributed.model.Product;
import pl.softwareskill.course.caching.distributed.model.ProductRepository;

import java.util.Collection;
import java.util.UUID;

@RequiredArgsConstructor
@RestController
@RequestMapping("/product")
class ProductController {

    private final ProductRepository productRepository;

    @GetMapping
    public Collection<Product> list() {
        return productRepository.findAll();
    }

    @PostMapping
    @ResponseStatus(code = HttpStatus.CREATED)
    public Product create(@RequestBody Product product) {
        product.setId(UUID.randomUUID());
        productRepository.add(product);
        return product;
    }

    @DeleteMapping("/{uuid}")
    public void remove(@PathVariable UUID uuid) {
        productRepository.remove(uuid);
    }
}

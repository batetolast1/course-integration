package pl.softwareskill.course.caching.distributed.config;

import org.infinispan.commons.marshall.JavaSerializationMarshaller;
import org.infinispan.configuration.cache.CacheMode;
import org.infinispan.configuration.cache.ConfigurationBuilder;
import org.infinispan.configuration.global.GlobalConfiguration;
import org.infinispan.configuration.global.GlobalConfigurationBuilder;
import org.infinispan.manager.DefaultCacheManager;
import org.infinispan.manager.EmbeddedCacheManager;
import org.infinispan.persistence.jdbc.configuration.JdbcStringBasedStoreConfigurationBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import pl.softwareskill.course.caching.distributed.model.Product;

import java.io.IOException;
import java.util.Map;
import java.util.UUID;

@Configuration
class InfinispanConfig {

    @Value("${cache.persistence.enabled:false}")
    private boolean cachePersistenceEnabled;

    @Bean
    Map<UUID, Product> productsCache() throws IOException {
        ConfigurationBuilder cb = new ConfigurationBuilder();

        cb.clustering().cacheMode(CacheMode.REPL_SYNC);
        cb.statistics().enable();

        if (cachePersistenceEnabled) {
            addPersistence(cb);
        }

        org.infinispan.configuration.cache.Configuration c = cb.build();
        EmbeddedCacheManager manager = new DefaultCacheManager(globalConfig());
        manager.defineConfiguration("replicatedProductsCache", c);
        return manager.getCache("replicatedProductsCache");
    }

    private GlobalConfiguration globalConfig() {
        GlobalConfigurationBuilder globalConfig = new GlobalConfigurationBuilder();

        globalConfig
                .cacheContainer()
                .statistics(true)
                .jmx()
                .enable();

        globalConfig
                .transport()
                .defaultTransport()
                .clusterName("qa-cluster")
                .addProperty("configurationFile", "default-configs/default-jgroups-udp.xml");

        globalConfig
                .serialization()
                .marshaller(new JavaSerializationMarshaller())
                .allowList()
                .addClasses(Product.class, UUID.class);

        return globalConfig.build();
    }

    private void addPersistence(ConfigurationBuilder cb) {
        cb.persistence()
                .addStore(JdbcStringBasedStoreConfigurationBuilder.class)
                .shared(true) // same data store for all nodes
                //.async() // write-behind (write through if not mentioned)
                .preload(true) // load data on startup
                .fetchPersistentState(false) // false, data will be transferred in shared cache over network
                .ignoreModifications(false)
                .purgeOnStartup(false)
                .table()
                    .dropOnExit(false)
                    .createOnStart(true)
                    .tableNamePrefix("ISPN_STRING_TABLE")
                    .idColumnName("ID_COLUMN").idColumnType("VARCHAR(255)")
                    .dataColumnName("DATA_COLUMN").dataColumnType("BINARY")
                    .timestampColumnName("TIMESTAMP_COLUMN").timestampColumnType("BIGINT")
                    .segmentColumnName("SEGMENT_COLUMN").segmentColumnType("INT")
                .connectionPool()
                .connectionUrl("jdbc:h2:file:./tmp/h2data/productsCache")
                .username("sa")
                .driverClass("org.h2.Driver");
    }
}

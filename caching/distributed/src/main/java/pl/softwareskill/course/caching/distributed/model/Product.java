package pl.softwareskill.course.caching.distributed.model;

import lombok.Data;

import java.io.Serializable;
import java.util.UUID;

@Data
public class Product implements Serializable {

    private UUID id;
    private String name;
}

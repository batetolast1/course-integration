package pl.softwareskill.course.caching.local;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import pl.softwareskill.course.caching.local.data.CachedDataSupplier;
import pl.softwareskill.course.caching.local.data.DataSupplier;

import java.time.Duration;

@Configuration
public class DataSupplierConfig {

    @Bean
    DataSupplier dataSupplier() {
        return new DataSupplier(Duration.ofSeconds(3));
    }

    @Bean
    CachedDataSupplier cachedDataSupplier(DataSupplier dataSupplier) {
        return new CachedDataSupplier(dataSupplier);
    }
}

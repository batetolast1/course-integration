package pl.softwareskill.course.caching.local;

import com.github.benmanes.caffeine.cache.RemovalCause;
import com.github.benmanes.caffeine.cache.RemovalListener;
import lombok.extern.slf4j.Slf4j;
import org.checkerframework.checker.nullness.qual.NonNull;
import org.checkerframework.checker.nullness.qual.Nullable;

@Slf4j
public class LoggingRemovalListener<K extends Object, V extends Object> implements RemovalListener<K, V> {

    @Override
    public void onRemoval(@Nullable K key, @Nullable V value, @NonNull RemovalCause cause) {
        log.info("Cache key has been removed: {}, reason: {}", key, cause);
    }
}

package pl.softwareskill.course.caching.local.data;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.time.Duration;
import java.time.Instant;
import java.util.Collection;
import java.util.UUID;
import java.util.concurrent.locks.LockSupport;

import static java.util.stream.Collectors.toList;

@Slf4j
@RequiredArgsConstructor
public class DataSupplier {

    private final Duration delay;

    public Data get(String key) {
        log.info("Loading data for key: {}", key);
        someDelay();
        log.info("Loaded data for key: {}", key);

        return createData(key);
    }

    public Collection<Data> getMany(Collection<String> keys) {
        log.info("Loading data for keys: {}", keys);
        someDelay();
        log.info("Loaded data for keys: {}", keys);

        return keys.stream().map(DataSupplier::createData).collect(toList());
    }

    private void someDelay() {
        LockSupport.parkNanos(delay.toNanos());
    }

    private static Data createData(String key) {
        String value = new StringBuilder()
                .append("For key: ")
                .append(key)
                .append(", value: ")
                .append(UUID.randomUUID().toString())
                .toString();

        return new Data(key, value, Instant.now());
    }
}

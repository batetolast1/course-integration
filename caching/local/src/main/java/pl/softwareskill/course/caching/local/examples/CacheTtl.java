package pl.softwareskill.course.caching.local.examples;

import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import pl.softwareskill.course.caching.local.LoggingRemovalListener;
import pl.softwareskill.course.caching.local.data.Data;
import pl.softwareskill.course.caching.local.data.DataSupplier;

import java.time.Duration;

@RestController
@RequiredArgsConstructor
public class CacheTtl {

    private final DataSupplier dataSupplier;
    private final Cache<String, Data> cache = Caffeine.newBuilder()
            .expireAfterWrite(Duration.ofSeconds(3))
            .removalListener(new LoggingRemovalListener<>())
            .build();

    @GetMapping("/ttl-eviction/{key}")
    public Data run(@PathVariable String key) {
        return cache.get(key, dataSupplier::get);
    }
}

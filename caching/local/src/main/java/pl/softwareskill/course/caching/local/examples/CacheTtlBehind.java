package pl.softwareskill.course.caching.local.examples;

import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import pl.softwareskill.course.caching.local.LoggingRemovalListener;
import pl.softwareskill.course.caching.local.data.Data;
import pl.softwareskill.course.caching.local.data.DataSupplier;

import java.time.Duration;

@RestController
public class CacheTtlBehind {

    private final DataSupplier dataSupplier;
    private final Cache<String, Data> cache;

    public CacheTtlBehind(DataSupplier dataSupplier) {
        this.dataSupplier = dataSupplier;

        this.cache = Caffeine.newBuilder()
                .refreshAfterWrite(Duration.ofSeconds(3))
                .removalListener(new LoggingRemovalListener<>())
                .build(dataSupplier::get);
    }

    @GetMapping("/ttl-eviction-behind/{key}")
    public Data run(@PathVariable String key) {
        return cache.get(key, dataSupplier::get);
    }
}

package pl.softwareskill.course.caching.local.examples;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import pl.softwareskill.course.caching.local.data.CachedDataSupplier;
import pl.softwareskill.course.caching.local.data.Data;

@RestController
@RequiredArgsConstructor
public class CachedMethod {

    private final CachedDataSupplier dataSupplier;

    @GetMapping("/method/{key}")
    public Data run(@PathVariable String key) {
        return dataSupplier.get(key);
    }
}

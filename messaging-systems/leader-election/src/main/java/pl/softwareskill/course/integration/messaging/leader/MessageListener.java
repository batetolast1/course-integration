package pl.softwareskill.course.integration.messaging.leader;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Configuration;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.messaging.Message;

@Slf4j
@Configuration
public class MessageListener {

    @JmsListener(destination = "orders")
    public void onMessage(Message<Object> message) {
        log.info("Received message: {}", message.getPayload());
    }
}

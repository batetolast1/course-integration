package pl.softwareskill.course.integration.messaging.jms;

import lombok.Getter;
import lombok.Setter;
import lombok.SneakyThrows;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.springframework.jms.support.JmsUtils;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Session;

public class JmsConfig {
    @Setter
    boolean sessionTransacted = false;
    @Setter
    int sessionAckMode = Session.AUTO_ACKNOWLEDGE;

    ConnectionFactory connectionFactory;
    Connection connection;
    @Getter
    Session session;

    @SneakyThrows
    public void init(String clientId) {
        connectionFactory = new ActiveMQConnectionFactory("tcp://localhost:61616");
        connection = connectionFactory.createConnection();
        connection.setClientID(clientId);
        connection.start();

        session = connection.createSession(sessionTransacted, sessionAckMode);
    }

    @SneakyThrows
    public void close() {
        JmsUtils.closeSession(session);
        JmsUtils.closeConnection(connection);
    }
}

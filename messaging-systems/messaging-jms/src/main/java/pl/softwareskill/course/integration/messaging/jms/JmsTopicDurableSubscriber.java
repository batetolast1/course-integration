package pl.softwareskill.course.integration.messaging.jms;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.jms.support.JmsUtils;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageListener;
import javax.jms.Session;
import javax.jms.Topic;

import static pl.softwareskill.course.integration.messaging.jms.TimeUtils.doInfiniteLoop;

@Slf4j
public class JmsTopicDurableSubscriber {

    public static void main(String[] args) {
        new JmsTopicDurableSubscriber().run();
    }

    public void run() {
        JmsConfig config = new JmsConfig();
        MessageConsumer consumer = null;

        try {
            var clientId = "JMS Durable Subscriber 1";
            config.init(clientId);
            Session session = config.getSession();

            log.info("Creating subscriber: {}", clientId);

            Topic destination = session.createTopic("orders-topic");
            consumer = session.createDurableSubscriber(destination, "durable-subscription");
            consumer.setMessageListener(new Listener(clientId));

            log.info("Listening...");

            doInfiniteLoop();
        } catch (JMSException e) {
            log.error("Error occurred", e);
        } finally {
            JmsUtils.closeMessageConsumer(consumer);
            config.close();
        }
    }

    @Slf4j
    @RequiredArgsConstructor
    static class Listener implements MessageListener {

        private final String clientId;

        @Override
        public void onMessage(Message message) {
            log.info("Client {} received message: {}", clientId, message);
        }
    }
}

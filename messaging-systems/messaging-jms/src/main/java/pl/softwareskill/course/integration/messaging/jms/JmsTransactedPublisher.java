package pl.softwareskill.course.integration.messaging.jms;

import lombok.extern.slf4j.Slf4j;
import org.springframework.jms.support.JmsUtils;

import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.Session;
import java.time.Duration;
import java.util.UUID;

import static java.time.Duration.ofSeconds;
import static pl.softwareskill.course.integration.messaging.jms.TimeUtils.waitForDuration;

@Slf4j
public class JmsTransactedPublisher {

    public static void main(String[] args) {
        new JmsTransactedPublisher().run();
    }

    public void run() {
        JmsConfig config = new JmsConfig();
        config.setSessionTransacted(true);
        MessageProducer producer = null;

        try {
            var clientId = "JMS Publisher " + UUID.randomUUID();
            config.init(clientId);

            Session session = config.getSession();
            log.info("Creating publisher {}", clientId);

            Destination destination = session.createQueue("orders");
            producer = session.createProducer(destination);

            producer.send(session.createTextMessage("message 1"));
            log.info("Message has been sent");
            waitForDuration(ofSeconds(3));

            producer.send(session.createTextMessage("message 2"));
            log.info("Message has been sent");
            waitForDuration(ofSeconds(3));

            session.commit();
            log.info("Comitted");
//            session.rollback();
//            log.info("Rolled back");
        } catch (JMSException e) {
            log.error("Error occurred", e);
        } finally {
            JmsUtils.closeMessageProducer(producer);
            config.close();
        }
    }
}

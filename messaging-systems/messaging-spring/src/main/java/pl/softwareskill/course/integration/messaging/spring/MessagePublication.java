package pl.softwareskill.course.integration.messaging.spring;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.softwareskill.course.integration.messaging.spring.dto.Order;

import java.util.UUID;

import static java.util.Collections.singletonMap;

@RestController
@RequestMapping("/orders")
@Slf4j
public class MessagePublication {

    @Autowired
    private JmsTemplate jmsTemplate;

    @PostMapping
    public ResponseEntity create() {
        var orderId = UUID.randomUUID();
        var message = "Order " + orderId;
//        var message = new Order();
//        message.setId(orderId.toString());
//        message.setDescription("Description");

        log.info("Sending order {}", orderId);
        jmsTemplate.convertAndSend("orders", message);

        return createResponse(orderId);

    }

    private ResponseEntity createResponse(UUID orderId) {
        return ResponseEntity
                .accepted()
                .body(singletonMap("orderId", orderId.toString()));
    }
}

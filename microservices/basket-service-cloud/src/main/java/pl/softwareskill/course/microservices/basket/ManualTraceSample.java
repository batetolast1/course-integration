package pl.softwareskill.course.microservices.basket;

import io.opentracing.Scope;
import io.opentracing.Span;
import io.opentracing.Tracer;
import io.opentracing.log.Fields;
import io.opentracing.tag.Tags;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.softwareskill.course.microservices.basket.samples.UnitOfWork;

import java.util.Map;
import java.util.UUID;

@RestController
@SpringBootApplication(scanBasePackages = "pl.softwareskill.course.microservices.basket.config")
@EnableAsync
@Import(Config.class)
public class ManualTraceSample {

    @Autowired
    Tracer tracer;

    @Autowired
    UnitOfWork unitOfWork;

    /**
     * Run with -Dspring.profiles.active=tracing
     * @param args
     */
    public static void main(String[] args) {
        SpringApplication.run(ManualTraceSample.class, args);
    }

    @GetMapping("/test")
    public String test() {
        Span span = tracer.buildSpan("someWork").start();

        try (Scope scope = tracer.scopeManager().activate(span)) {
            unitOfWork.doSomeWork(UUID.randomUUID().toString());
        } catch(Exception ex) {
            Tags.ERROR.set(span, true);
            span.log(Map.of(Fields.EVENT, "error", Fields.ERROR_OBJECT, ex, Fields.MESSAGE, ex.getMessage()));
        } finally {
            span.finish();
        }

        return "test";
    }

    @GetMapping("/test2")
    public String test2() throws InterruptedException {
        unitOfWork.doSomeWorkMonitored(UUID.randomUUID().toString());
        return "test";
    }

    @GetMapping("/test3")
    public String test3() throws InterruptedException {
        unitOfWork.doSomeAsyncWork(UUID.randomUUID().toString());
        return "test";
    }
}

@Configuration
class Config {

    @Bean
    public UnitOfWork unitOfWork() {
        return new UnitOfWork();
    }
}
package pl.softwareskill.course.microservices.basket.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.loadbalancer.annotation.LoadBalancerClient;
import org.springframework.cloud.loadbalancer.core.ServiceInstanceListSupplier;
import org.springframework.cloud.loadbalancer.core.ServiceInstanceListSupplier.FixedServiceInstanceListSupplier.SimpleBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;
import org.springframework.web.reactive.function.client.WebClient;

@Configuration
@Profile("!consul")
@LoadBalancerClient(name = "orders-service", configuration = OrderServiceFixedInstancesConfig.class)
public class OrderServiceFixedInstancesConfig {

    @Bean
    @Primary
    public ServiceInstanceListSupplier fixedListSupplier(@Value("${order.service.urls}") String[] urls) {
        SimpleBuilder builder = ServiceInstanceListSupplier.fixed("orders-service");

        for (String url : urls) {
            String[] part = url.split(":");
            builder.instance(part[0], Integer.valueOf(part[1]));
        }

        return builder.build();
    }

    @Bean
    @LoadBalanced
    public WebClient.Builder orderServiceWebClient() {
        return WebClient.builder();
    }
}

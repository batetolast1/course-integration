package pl.softwareskill.course.microservices.basket;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.softwareskill.course.microservices.basket.service.OrderService;

import java.util.UUID;

@Slf4j
@RestController
@RequestMapping("/simple")
@RequiredArgsConstructor
public class SimpleClientController {

    private final OrderService orderService;

    @GetMapping
    public String createOrder() {
        log.info("Creating order");

        try {
            UUID createdOrderId = orderService.createOrder("Some order");
            log.info("Created order id: {}", createdOrderId);

            return "Created order: " + createdOrderId;
        } catch (Exception e) {
            log.error("Error during creating order", e);

            return "Error";
        }
    }
}

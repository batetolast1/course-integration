package pl.softwareskill.course.microservices.basket.service;

import java.util.UUID;
import lombok.Builder;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.springframework.web.client.RestTemplate;

@RequiredArgsConstructor
public class OrderService {

    private final RestTemplate restTemplate;
    private final String baseUrl;

    public UUID createOrder(String description) {
        Request request = Request.builder()
                .description(description)
                .build();

        return restTemplate.postForEntity(baseUrl + "/orders", request, Response.class)
                .getBody()
                .getOrderId();
    }

    @Data
    @Builder
    private static class Request {
        private String description;
    }

    @Data
    private static class Response {
        private UUID orderId;
    }
}

package pl.softwareskill.course.microservices.orders.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import pl.softwareskill.course.microservices.orders.domain.OrderRepository;
import pl.softwareskill.course.microservices.orders.infra.InMemoryOrderRepository;

@Configuration
public class OrderRepositoryConfig {

    @Bean
    public OrderRepository orderRepository() {
        return new InMemoryOrderRepository();
    }
}

package pl.softwareskill.course.microservices.orders.rest.dto;

import lombok.Data;

@Data
public class CreateOrderDto {
    String description;
}

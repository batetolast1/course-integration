package pl.softwareskill.course.microservices.orders.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import pl.softwareskill.course.microservices.orders.domain.CreateOrderService;
import pl.softwareskill.course.microservices.orders.domain.OrderRepository;

@Configuration
public class OrderConfig {

    @Bean
    public CreateOrderService createOrderService(OrderRepository orderRepository) {
        return new CreateOrderService(orderRepository);
    }
}

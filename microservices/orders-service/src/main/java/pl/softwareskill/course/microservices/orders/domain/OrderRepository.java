package pl.softwareskill.course.microservices.orders.domain;

import java.util.Optional;
import java.util.UUID;

public interface OrderRepository {

    Optional<Order> find(UUID orderId);

    void save(Order order);

    void remove(UUID orderId);
}
